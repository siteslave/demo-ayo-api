import * as knex from 'knex';

export class ServiceModel {

  search(db: knex, cid: any) {
    var sql = `
SELECT
  p.gw_hospcode, oc.hospitalname, p.pname as title_name,
  p.fname as first_name, p.lname as last_name,
  p.birthday, p.sex, p.cid, p.hn
FROM patient p
LEFT OUTER JOIN opdconfig oc ON p.gw_hospcode=oc.gw_hospcode
WHERE p.cid=?
GROUP BY p.gw_hospcode, p.cid;
    `;

    return db.raw(sql, [cid])
  }

  service(db: knex, hospcode: any, hn: any) {
    var sql = `
  SELECT
	o.gw_hospcode,
	oc.hospitalname,
	o.hn,
	o.vn,
	o.vstdate AS visit_date,
	o.vsttime AS visit_time,
	o.main_dep AS main_department,
	o.spclty AS sub_department,
	pt.NAME AS pttype,
	pn.pttypeno AS pttype_no
FROM
	ovst o
	LEFT OUTER JOIN opdconfig oc ON o.gw_hospcode = oc.gw_hospcode
	LEFT OUTER JOIN pttype pt ON o.gw_hospcode = pt.gw_hospcode
	AND o.pttype = pt.pttype
	LEFT OUTER JOIN pttypeno pn ON pn.gw_hospcode = pt.gw_hospcode
	AND pt.pttype = pn.pttype
	AND o.pttype = pt.pttype
WHERE o.gw_hospcode=?  AND o.hn=?
GROUP BY
	o.gw_hospcode,
	o.vn;
    `;

    return db.raw(sql, [hospcode, hn])
  }

  opdscreen(db: knex, hospcode: any, hn: any, vn: any) {
    var sql = `
    SELECT o.gw_hospcode,oc.hospitalname,o.hn,o.vn,o.bps,o.bpd,o.bw as weight,o.height,o.bmi,o.waist,o.hr as heart_rate,o.pulse as pulse_rate,o.rr as restpiratory_rate,o.temperature,o.hpi  from opdscreen o
    left outer join opdconfig oc on o.gw_hospcode=oc.gw_hospcode
    where o.gw_hospcode=? and o.hn=? and o.vn=?
    group by o.gw_hospcode,o.vn;
`;

    return db.raw(sql, [hospcode, hn, vn])
  }

  drug(db: knex, hospcode: any, hn: any, vn: any) {
    var sql = `
    SELECT o.gw_hospcode,oc.hospitalname,o.hn,o.vn,o.icode as drug_code,d.name as drug_name,o.drugusage,o.qty,du.name1 as usage1,du.name2 as usage2,du.name3 as usage3 from opitemrece o
    left outer join opdconfig oc on o.gw_hospcode=oc.gw_hospcode
    left outer join drugitems d on o.gw_hospcode=d.gw_hospcode and o.icode=d.icode
    left outer join drugusage du on o.gw_hospcode=du.gw_hospcode and o.drugusage=du.drugusage
    WHERE SUBSTR(o.icode,1,1)=1
    AND o.gw_hospcode=? and o.hn=? and o.vn=?

    group by o.gw_hospcode,o.vn,o.icode;
`;

    return db.raw(sql, [hospcode, hn, vn])
  }

  diagnosis(db: knex, hospcode: any, hn: any, vn: any) {
    var sql = `
    SELECT od.gw_hospcode,oc.hospitalname,od.hn,od.vn,od.icd10 as diagnosis_code,od.diagtype as diagnosis_type from ovstdiag od
    left outer join opdconfig oc on od.gw_hospcode=oc.gw_hospcode
    where left(od.icd10,1) not in (1,2,3,4,5,6,7,8,9)
    AND od.gw_hospcode=? and od.hn=? and od.vn=?
    group by od.gw_hospcode,od.vn, od.icd10, od.diagtype;
    `;

    return db.raw(sql, [hospcode, hn, vn])
  }

  appointment(db: knex, hospcode: any, hn: any) {
    var sql = `
    SELECT
	oa.gw_hospcode,
	oc.hospitalname,
	oa.hn,
	oa.vn,
	oa.vstdate AS visit_date,
	oa.nextdate AS appointment_date,
	oa.nexttime AS appointment_time,
	oa.app_cause AS cause,
	oa.note
FROM
	oapp oa
	LEFT OUTER JOIN opdconfig oc ON oa.gw_hospcode = oc.gw_hospcode
where oa.gw_hospcode=? and oa.hn=?
GROUP BY
	oa.gw_hospcode,
	oa.vn,
	oa.oapp_id
ORDER BY oa.nextdate DESC;
    `;

    return db.raw(sql, [hospcode, hn])
  }

  patient(db: knex, hospcode: any, hn: any) {
    var sql = `
    SELECT p.gw_hospcode,oc.hospitalname,p.pname as title_name,p.fname as first_name,p.lname as last_name,p.birthday,p.sex,p.cid,p.hn from patient p
    left outer join opdconfig oc on p.gw_hospcode=oc.gw_hospcode
    where p.gw_hospcode=? and p.hn=?
    group by p.gw_hospcode,p.cid;
    `;
    return db.raw(sql, [hospcode, hn])
  }

}