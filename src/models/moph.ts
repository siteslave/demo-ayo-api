const axios = require("axios").default;

export class MophModel {
  search(cid: any, token: any) {
    var options = {
      method: 'GET',
      url: 'http://203.157.103.178/api/v1/search/' + cid,
      headers: {
        Authorization: 'Bearer ' + token
      }
    };

    return new Promise((resolve: any, reject: any) => {
      axios.request(options).then(function (response: any) {
        resolve(response.data);
      }).catch(function (error: any) {
        reject(error);
      });
    });
  }
}