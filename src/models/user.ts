import * as knex from 'knex';

export class UserModel {

  list(db: knex) {
    return db('user')
      .select("id", "firstname", "lastname", "email", "cid")
  }

  create(user: any, db: knex) {
    return db('user').insert(user)
  }

  update(id: any, user: any, db: knex) {
    return db('user').update(user).where('id', id);
  }

  delete(id: any, db: knex) {
    return db('user').where('id', id).del();
  }

}