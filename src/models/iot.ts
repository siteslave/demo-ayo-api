import * as knex from 'knex';

export class IoTModel {

  getBp(db: knex, cid: any) {
    const sql = `
    select cid
    ,substring_index(GROUP_CONCAT(pulse_value ORDER BY request_timestamp desc),',',1) as pulse
    ,substring_index(GROUP_CONCAT(bp_dia_value ORDER BY request_timestamp desc),',',1) as bpd
    ,substring_index(GROUP_CONCAT(bp_sys_value ORDER BY request_timestamp desc),',',1) as bps
    ,substring_index(GROUP_CONCAT(create_date ORDER BY request_timestamp desc),',',1) as create_date

    from history_health
    where cid=?
    and bp_sys_value>0 and bp_dia_value>0
    GROUP BY cid,date(request_timestamp)
    order by request_timestamp desc
    limit 5;`;
    return db.raw(sql, [cid])
  }

}